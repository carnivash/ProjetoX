package com.app.sigam;

import at.nineyards.anyline.models.AnylineImage;

public class Carro {

    private String result, marcaModelo;
    private AnylineImage cutoutImage;
    private AnylineImage fullImage;
    int situacao;
    public int getSituacao() {
        return situacao;
    }

    public void setSituacao(int situacao) {
        this.situacao = situacao;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public AnylineImage getCutoutImage() {
        return cutoutImage;
    }

    public void setCutoutImage(AnylineImage cutoutImage) {
        this.cutoutImage = cutoutImage;
    }

    public AnylineImage getFullImage() {
        return fullImage;
    }

    public void setFullImage(AnylineImage fullImage) {
        this.fullImage = fullImage;
    }


    @Override
    public boolean equals(Object obj) {


            // TODO Auto-generated method stub
            if(obj instanceof Carro)
            {
                Carro temp = (Carro) obj;
                if(this.result.equals(temp.result))
                    return true;
            }
            return false;
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub

        return (this.result.hashCode());
    }

    public String getMarcaModelo() {
        return marcaModelo;
    }

    public void setMarcaModelo(String marcaModelo) {
        this.marcaModelo = marcaModelo;
    }
}
