package com.app.sigam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.sigam.DBController;

public class DBAdapter {

    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DBController mDbHelper;

    public DBAdapter(Context context)
    {
        this.mContext = context;
        mDbHelper = new DBController(mContext);
    }

    public DBAdapter createDatabase() throws SQLException
    {
        mDbHelper.createDataBase();
        return this;
    }

    public DBAdapter open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close()
    {
        mDbHelper.close();
    }

    public Cursor getTestData(){
        try
        {
            String sql ="SELECT * FROM veiuculos";

            Cursor mCur = mDb.rawQuery(sql, null);
            if (mCur!=null)
            {
                mCur.moveToNext();
            }
            return mCur;
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "getTestData >>"+ mSQLException.toString());
            throw mSQLException;
        }
    }


    public List<Carro> getPlaca() {

        List<Carro> recordsList = new ArrayList<>();

        // select query
        String sql = "SELECT PLACA, VEICULO, STATUS FROM veiuculos";

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // int productId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(fieldProductId)));
                Carro carro = new Carro();
                carro.setResult(cursor.getString(cursor.getColumnIndex("PLACA")));
                carro.setSituacao(cursor.getInt(cursor.getColumnIndex("STATUS")));
                carro.setMarcaModelo(cursor.getString(cursor.getColumnIndex("VEICULO")));
                // add to list
                recordsList.add(carro);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }
}