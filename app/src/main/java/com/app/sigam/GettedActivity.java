package com.app.sigam;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class GettedActivity extends AppCompatActivity {

    Bundle extras;
    int tipo;
    String placa;
    String letrasStr, numerosStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getted);

        extras = getIntent().getExtras();
        tipo = extras.getInt("valor");
        placa = extras.getString("placa");
        Bitmap imagem = MainActivity.listaLimpa.get(tipo).getCutoutImage().getBitmap();

        TextView letras, numeros;
        letras = findViewById(R.id.main_placa_letras);
        numeros = findViewById(R.id.main_placa_numeros);

        letrasStr = placa.substring(0, 3);
        numerosStr = placa.substring(3);
        letras.setText(letrasStr);
        numeros.setText(numerosStr);

        View ipvaAtrasado = findViewById(R.id.main_ipva_atrasado);
        View ipvaOk = findViewById(R.id.main_btn_ipva_ok);
        View verTudo = findViewById(R.id._button_ver_tudo);

        ImageView imageResult= findViewById(R.id.main_image_result);

        imageResult.setImageBitmap(imagem);

        if(tipo % 2 == 0){
            ipvaAtrasado.setVisibility(View.VISIBLE);
            ipvaOk.setVisibility(View.GONE);
        }else{
            ipvaAtrasado.setVisibility(View.GONE);
            ipvaOk.setVisibility(View.VISIBLE);        }

        verTudo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ResultActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        .putExtra("valor", tipo)
                        .putExtra("placa", placa));            }
        });
    }
}
