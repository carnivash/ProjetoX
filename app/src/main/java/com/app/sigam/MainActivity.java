package com.app.sigam;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static Carro carro;
    private static int idCarro;
    Toolbar toolbar;
    static HashSet<Carro> list;
    static List<Carro> listaLimpa;
    static View botaoVermelho, botaoVerde;
    static TextView cidadeEstado, carroModelo, placaLetras, placaNumeros;
    static ImageView resultMain_fotos;
    //    ListView lista;
    RecyclerView rv;
    LinearLayoutManager lm;

    private static final int REQ_CODE = 1001;
    private RecyclerAdapter adapter;
    private static String strPlaca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = new HashSet<>();

        initComponents();

        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

    }

    private void initComponents() {
//        lista = findViewById(R.id.image_list);

        lm = new LinearLayoutManager(getApplicationContext());
        rv = findViewById(R.id.image_list);

        botaoVerde = findViewById(R.id.main_btn_ipva_ok);
        botaoVermelho = findViewById(R.id.main_ipva_atrasado);
        cidadeEstado = findViewById(R.id.main_cidade_estado);
        carroModelo = findViewById(R.id.main_carro_modelo);
        placaLetras = findViewById(R.id.main_placa_letras);
        placaNumeros = findViewById(R.id.main_placa_numeros);
        resultMain_fotos = findViewById(R.id.main_image_result);
        toolbar = findViewById(R.id.main_toolbar);

        botaoVermelho.setVisibility(View.GONE);
        botaoVerde.setVisibility(View.GONE);
        cidadeEstado.setVisibility(View.GONE);
        carroModelo.setVisibility(View.GONE);
        placaNumeros.setVisibility(View.GONE);
        placaLetras.setVisibility(View.GONE);
        resultMain_fotos.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        TextView t = findViewById(R.id.tv_lista_vazia);
        ImageView logo = findViewById(R.id.logo);
        if (list.isEmpty()) {
            t.setVisibility(View.VISIBLE);
            logo.setVisibility(View.VISIBLE);
        } else {
            t.setVisibility(View.GONE);
            logo.setVisibility(View.GONE);
        }

        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        listaLimpa = new ArrayList<>(list);
        rv.setLayoutManager(lm);
        adapter = new RecyclerAdapter(listaLimpa, getApplicationContext());

        rv.setAdapter(adapter);

        botaoVermelho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ResultActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        .putExtra("valor", idCarro)
                        .putExtra("placa", strPlaca)
                );
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbarmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.camera) {
            startActivity(new Intent(getApplicationContext(), ScannerActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);

    }


    public static void updateView(int position){

        carro = listaLimpa.get(position);
        strPlaca = listaLimpa.get(position).getResult();
        idCarro = position;
        resultMain_fotos.setImageBitmap(listaLimpa.get(position).getCutoutImage().getBitmap());

        cidadeEstado.setText("Recife - PE");
        carroModelo.setText("Fiat mobi 2012/2012 - Vermelho");
        placaLetras.setText(listaLimpa.get(position).getResult().substring(0,3));
        placaNumeros.setText(listaLimpa.get(position).getResult().substring(3));

        resultMain_fotos.setVisibility(View.VISIBLE);
        cidadeEstado.setVisibility(View.VISIBLE);
        carroModelo.setVisibility(View.VISIBLE);
        placaLetras.setVisibility(View.VISIBLE);
        placaNumeros.setVisibility(View.VISIBLE);

        if(listaLimpa.get(position).getSituacao() == 0){
            botaoVerde.setVisibility(View.VISIBLE);
            botaoVermelho.setVisibility(View.GONE);
        }else{
            botaoVerde.setVisibility(View.GONE);
            botaoVermelho.setVisibility(View.VISIBLE);
        }

    }
}