package com.app.sigam;

import android.view.View;

public interface OnImageItemClickListener {

    void onClick(View v, int position, boolean isLongClick);

}
