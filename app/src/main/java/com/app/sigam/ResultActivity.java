package com.app.sigam;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Bundle extras = getIntent().getExtras();
        int tipo = extras.getInt("valor");
        String placa = extras.getString("placa");
        Bitmap imagem = MainActivity.listaLimpa.get(tipo).getCutoutImage().getBitmap();

        TextView t = findViewById(R.id.txt_valor_parcela_cartao);
        TextView placa1 = findViewById(R.id.main_cidade_estado);

        placa1.setText(placa);

        ImageView i = findViewById(R.id.main_image_result);

        if (imagem != null) {
            Log.i("RESULT", "imagem ok");
            i.setImageBitmap(imagem);
        } else {
            Log.i("RESULT", "imagem vaiza");
        }


        ListView listView = findViewById(R.id._dynamic);
        List<String> l = new ArrayList<>();

        ImageView imgPagar = findViewById(R.id.img_pagar);
        ImageView imgAutuar = findViewById(R.id.img_autuar);

        l.add("R$ 1200,00");
        l.add("R$ 400,00");
        l.add("R$ 400,00");
        l.add("R$ 400,00");

        listView.setAdapter(new ArrayAdapter<>(this, R.layout.list_item_parcelas, R.id.txt_valor_parcela_cartao, l));


        t.setText(l.get(0));

        AlertDialog.Builder alertB = new AlertDialog.Builder(this);
        alertB.setTitle("Autuação de veículo");
        alertB.setMessage("Veículo Autuado");
        alertB.setCancelable(false);
        alertB.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        final AlertDialog alert = alertB.create();

        imgAutuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.show();
            }
        });

        imgPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PagamentoActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));

            }
        });

    }
}
