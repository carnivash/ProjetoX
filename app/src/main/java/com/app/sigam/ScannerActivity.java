package com.app.sigam;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import at.nineyards.anyline.camera.CameraController;
import at.nineyards.anyline.camera.CameraOpenListener;
import io.anyline.plugin.ScanResultListener;
import io.anyline.plugin.licenseplate.LicensePlateScanResult;
import io.anyline.plugin.licenseplate.LicensePlateScanViewPlugin;
import io.anyline.view.BaseScanViewConfig;
import io.anyline.view.ScanView;
import io.anyline.view.ScanViewPluginConfig;

public class ScannerActivity extends AppCompatActivity implements CameraOpenListener {
    private ScanView scanView;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_scanner);

        String license = getString(R.string.anyline_license_key);

        // Get the view from the layout
        scanView = findViewById(R.id.scanner);
        Button fechar = findViewById(R.id.button_fechar_scanner);
        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        String configFile = "ocr_general.json";

        //init the scanViewPlugin configuration which hold the scan view ui configuration (cutoutConfig and ScanFeedbackConfig)
        ScanViewPluginConfig licensePlateScanviewPluginConfig = new ScanViewPluginConfig(getApplicationContext(), configFile);

        //init the scan view
        LicensePlateScanViewPlugin scanViewPlugin = new LicensePlateScanViewPlugin(getApplicationContext(), license, licensePlateScanviewPluginConfig, "LICENSE_PLATE");

        //init the base scanViewconfig which hold camera and flash configuration
        BaseScanViewConfig licensePlateBaseScanViewConfig = new BaseScanViewConfig(getApplicationContext(), configFile);

        //set the base scanViewConfig to the ScanView
        scanView.setScanViewConfig(licensePlateBaseScanViewConfig);

        //set the scanViewPlugin to the ScanView
        scanView.setScanViewPlugin(scanViewPlugin);

        //add result listener
        scanViewPlugin.addScanResultListener(new ScanResultListener<LicensePlateScanResult>() {
            @Override
            public void onResult(LicensePlateScanResult result) {


                Carro carro = new Carro();

                carro.setCutoutImage(result.getCutoutImage());
                carro.setFullImage(result.getFullImage());
                carro.setResult(result.getResult());

                MainActivity.list.add(carro);
                scanView.start();

                Log.i("RESULT", "ADICIONADO");

            }
        });

    }

    private void startScanning() {
        // this must be called in onResume, or after a result to start the scanning again
        if (!scanView.getScanViewPlugin().isRunning()) {
            scanView.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startScanning();
    }

    @Override
    protected void onPause() {
        super.onPause();

        scanView.stop();
        scanView.releaseCameraInBackground();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCameraOpened(CameraController cameraController, int i, int i1) {

    }

    @Override
    public void onCameraError(Exception e) {
        e.printStackTrace();
    }

}
